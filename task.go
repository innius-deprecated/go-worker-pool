package pool

type Task interface {
	Execute()
}
