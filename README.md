# go-worker-pool

A dynamically-resizable worker pool.

Based on http://stackoverflow.com/questions/23837368/idiomatic-variable-size-worker-pool-in-go

# Usage
```go
type myTask struct {
}

func (m *myTask) Execute() {
	// Do something
}

p_size := c.GetInt("foo", 3)
p := pool.NewDynamicPool(p_size, 5)

p.Exec(&myTask{})
p.Exec(&myTask{})
```
