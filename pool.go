package pool

type Pool interface {
	// Close the pool, disabling execution of new tasks
	Close()
	// Execute a new task on the pool. Will block if there are no free workers available.
	Exec(Task)
}
