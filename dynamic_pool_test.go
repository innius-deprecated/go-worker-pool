package pool_test

import (
	"bitbucket.org/to-increase/go-config"
	"bitbucket.org/to-increase/go-worker-pool"
	"github.com/stretchr/testify/assert"
	"sync/atomic"
	"testing"
	"time"
)

type myTask struct {
	cnt *uint64
}

func (m *myTask) Execute() {
	atomic.AddUint64(m.cnt, 1)
}

type mySlowTask struct {
	start *uint64
	end   *uint64
}

func (m *mySlowTask) Execute() {
	atomic.AddUint64(m.start, 1)
	time.Sleep(75 * time.Millisecond)
	atomic.AddUint64(m.end, 1)
}

func TestPoolCompletion(t *testing.T) {
	c := config.NewConfigurator()
	p_size := c.GetInt("foo", 3)
	p := pool.NewDynamicPool(p_size, 5)
	var cnt uint64 = 0
	var expected uint64 = 10

	for i := uint64(0); i < expected; i++ {
		p.Exec(&myTask{
			cnt: &cnt,
		})
	}

	time.Sleep(50 * time.Millisecond)
	assert.Equal(t, expected, atomic.LoadUint64(&cnt))
}

func TestPoolDelayed(t *testing.T) {
	c := config.NewConfigurator()

	p_size := c.GetInt("foo", 3)
	p := pool.NewDynamicPool(p_size, 5)

	var startcnt uint64 = 0
	var endcnt uint64 = 0

	for i := uint64(0); i < uint64(10); i++ {
		p.Exec(&mySlowTask{
			start: &startcnt,
			end:   &endcnt,
		})
	}

	time.Sleep(50 * time.Millisecond)
	assert.Equal(t, uint64(6), atomic.LoadUint64(&startcnt))
	assert.Equal(t, uint64(3), atomic.LoadUint64(&endcnt))
}
