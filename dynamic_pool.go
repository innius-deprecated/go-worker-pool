package pool

import (
	"bitbucket.org/to-increase/go-config/types"
	"sync"
)

type pool struct {
	mu    sync.Mutex
	size  int
	tasks chan Task
	kill  chan struct{}
	wg    sync.WaitGroup
}

func NewDynamicPool(poolsize types.DynamicIntValue, queuesize int) Pool {
	pool := &pool{
		tasks: make(chan Task, queuesize),
		kill:  make(chan struct{}),
	}

	pool.resize(poolsize.Value())

	poolsize.OnChange(func(p types.Path, value int) {
		pool.resize(value)
	})

	return pool
}

func (p *pool) worker() {
	defer p.wg.Done()
	for {
		select {
		case task, ok := <-p.tasks:
			if !ok {
				return
			}
			task.Execute()
		case <-p.kill:
			return
		}
	}
}

func (p *pool) resize(n int) {
	p.mu.Lock()
	defer p.mu.Unlock()
	for p.size < n {
		p.size++
		p.wg.Add(1)
		go p.worker()
	}
	for p.size > n {
		p.size--
		p.kill <- struct{}{}
	}
}

func (p *pool) Close() {
	close(p.tasks)
	p.resize(0)
}

func (p *pool) Exec(task Task) {
	p.tasks <- task
}
